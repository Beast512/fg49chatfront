import React from 'react';
import {ListGroup} from "react-bootstrap";
import CommentItem from "./CommentItem";
import {channelStatistic} from "../util";

const Comments = ({messages}) => {
  const stat = channelStatistic(messages);

  const getCount = (channelId) => (
    stat.get(channelId)?.count
  );

  return(
    <ListGroup>
      {
        messages.length === 0
          ? <ListGroup.Item>Пока нет сообщений в чате :(</ListGroup.Item>
          : messages
            .slice(0, 7)
            .map((message) => (
              <CommentItem
                key={message.id}
                message={message}
                count={getCount(messages.author.channelId)}
              />
            ))
      }
    </ListGroup>
  );
};

export default Comments;