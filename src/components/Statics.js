import React from 'react';
import {ListGroup} from "react-bootstrap";
import StaticItem from "./StaticItem";
import {channelStatistic} from "../util";

const Statics = ({messages}) => {
  const items = Array.from(channelStatistic(messages).values())
    .sort((a, b) => b.count - a.count);

  return (
    <ListGroup>
      {
        messages && items.map((item) => (
          <StaticItem imageUrl={item.imageUrl} name={item.name} count={item.count}/>
        ))
      }
    </ListGroup>
  );
};

export default Statics;