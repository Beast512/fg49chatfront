import React from 'react';
import {Badge, Col, ListGroup, Row} from "react-bootstrap";
import FGAvatar from "./FGAvatar";

const CommentItem = ({message, count}) => (
  <ListGroup.Item>
    <Row style={{minWidth: 91}}>
      <Col xs={2} md={2}>
        <FGAvatar url={message.author.imageUrl}/>
      </Col>
      <Col xs={10} md={10} className="d-flex justify-content-between align-items-start">
        <div className="ms-2 me-auto">
          <div className="fw-bold">{message.author.name}</div>
          {message.message}
        </div>
        <Badge bg="primary" pill>{count}</Badge>
      </Col>
    </Row>
  </ListGroup.Item>
);

export default CommentItem;