export const channelStatistic = (messages) => (
  messages.reduce(
    (map, message) => {
      const channelId = message.author.channelId;

      if (map.has(channelId)) {
        map.set(
          channelId,
          {
            ...map.get(channelId),
            count: map.get(channelId) + 1,
          },
        );
      } else {
        map.set(
          channelId,
          {
            ...message.author,
            count: 1,
          },
        );
      }
    },
    new Map(),
  )
);
