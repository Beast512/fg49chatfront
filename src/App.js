import React, {useState, useEffect} from 'react';

import {Container} from "react-bootstrap";
import Comments from "./components/Comments";
import FGNavbar from "./components/FGNavbar";
import Statics from "./components/Statics";

const CLOSE_APPLICATION = 1000;

function App() {
    const socket = useRef(null);
    const [state, setState] = useState('chat');
    const [messages, setMessages] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    const getConnection = () => {
        setIsLoading(true);
        const ws = new WebSocket("ws://localhost:8766");

        ws.onmessage = ({data}) => {
            const message = JSON.parse(data);

            if (message?.type === 'textMessage') {
                setMessages((messages) => ([JSON.parse(data), ...messages]))
            }
        };
        ws.onopen = () => {
            setIsLoading(false);
        };
        ws.onclose = (event) => {
            if (event.code !== CLOSE_APPLICATION) {
                socket.current = getConnection();
            }
        }

        return ws;
    };

    useEffect(() => {
        socket.current = getConnection();

        return () => {
            if (socket.current) {
                socket.current.close(CLOSE_APPLICATION);
            }
        };
    }, []);

    return (
        <Container fluid>
            <FGNavbar onPageChange={setState}/>
            {
                isLoading
                  ? <Spinner />
                  : {
                      'chat': <Comments messages={messages} setMessages={setMessages}/>,
                      'static': <Statics messages={messages} />,
                  }[state]
            }
        </Container>
    );
}

export default App;
